package com.oe.todo.repositories

import com.oe.todo.db.TodoDatabase
import com.oe.todo.models.Todo

class TodoRepo(private val db: TodoDatabase) {

    suspend fun insertTodo(item: Todo) = db.todoDao.insertTodo(item)

    suspend fun deleteTodo(item: Todo) = db.todoDao.deleteTodo(item)

    fun getAllTodo() = db.todoDao.getAllTodos()
}