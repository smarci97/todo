package com.oe.todo.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.oe.todo.models.Todo
import com.oe.todo.repositories.TodoRepo
import kotlinx.coroutines.launch

class MainViewModel(private val repository : TodoRepo) : ViewModel() {

    fun getAllTodo() : LiveData<List<Todo>> = repository.getAllTodo()

    fun deleteTodo(todo : Todo) = viewModelScope.launch {
        repository.deleteTodo(todo)
    }

    fun insertTodo(todo : Todo) = viewModelScope.launch {
        repository.insertTodo(todo)
    }
}