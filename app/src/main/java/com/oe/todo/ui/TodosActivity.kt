package com.oe.todo.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import com.oe.todo.R
import com.oe.todo.db.TodoDatabase
import com.oe.todo.models.Todo
import com.oe.todo.repositories.TodoRepo
import kotlinx.android.synthetic.main.activity_todos.*
import kotlinx.android.synthetic.main.dialog_add.view.*

class TodosActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel
    private var todosAdapter: TodosActivityAdapter = TodosActivityAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_todos)

        val repository = TodoRepo(TodoDatabase(this))
        viewModel = MainViewModel(repository)

        addTodo()
        updateUI()
    }

    private fun addTodo() {
        fab_add.setOnClickListener {
            showBottomSheetDialog()
        }
    }

    private fun showBottomSheetDialog() {
        val bsd = layoutInflater.inflate(R.layout.dialog_add, null)
        val dialog = BottomSheetDialog(this)
        dialog.setContentView(bsd)

        bsd.btn_save.setOnClickListener {
            val todo = Todo(title = bsd.add_title.text.toString(), checked = false)
            viewModel.insertTodo(todo)
            Snackbar.make(findViewById(R.id.constraintlayout), "Todo saved.", Snackbar.LENGTH_SHORT)
                .show()
            dialog.dismiss()
        }

        bsd.btn_cancel.setOnClickListener {
            dialog.dismiss()
        }

        bsd.btn_save.showSoftInputOnFocus

        dialog.show()
    }

    private fun updateUI() {
        recyclerview_todo.apply {
            adapter = todosAdapter
            layoutManager = LinearLayoutManager(context)
        }

        refreshTodoList()

        todosAdapter.deleteClick = {
            viewModel.deleteTodo(it)
        }
    }

    private fun refreshTodoList() {
        viewModel.getAllTodo().observe(this, Observer { todos ->
            todos?.let {
                todosAdapter.setData(it as MutableList<Todo>)
            }
        })
    }

}