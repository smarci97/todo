package com.oe.todo.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.oe.todo.models.Todo

@Dao
interface TodoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTodo(todo : Todo)

    @Delete
    suspend fun deleteTodo(todo: Todo)

    @Query("SELECT * FROM todos")
    fun getAllTodos() : LiveData<List<Todo>>
}